import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.data = None
        self.retrieved = False

    def retrieve(self):
        if not self.retrieved:
            print('Descargando url')
            file, headers = urllib.request.urlretrieve(self.url)
            with open(file, 'r') as f:
                self.data = f.read()
            self.retrieved = True

    def show(self):
        if self.data is None:
            print("No se ha descargado nada")
            return
        print(self.data)

    def content(self):
        cad = self.data
        return cad


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url)
            robot.retrieve()
            self.cache[url] = robot

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for i in self.cache:
            print(i)


if __name__ == '__main__':

    cache = Cache()
    cache.retrieve('https://www.python.org/')
    cache.show('https://www.python.org/')
    cache.retrieve("https://www.aulavirtual.urjc.es/moodle/")
    cache.show_all()
